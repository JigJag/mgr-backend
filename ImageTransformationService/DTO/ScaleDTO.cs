﻿
namespace ImageTransformationService.DTO
{
    public class ScaleDTO
    {
        public string ImageData { get; set; }
        public int ContainerWidth { get; set; }
        public int ContainerHeight { get; set; }
        public double Percent { get; set; }
    }
}
