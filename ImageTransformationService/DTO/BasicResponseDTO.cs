﻿
namespace ImageTransformationService.DTO
{
    public class BasicResponseDTO
    {
        public string Image { get; set; }
        public bool Ok { get; set; }
        public string Message { get; set; }
    }
}
