﻿
using ImageTransformationService.DTO;
using System.Threading.Tasks;

namespace ImageTransformationService.Services.Interfaces
{
    public interface IImageRotationService
    {
        Task<BasicResponseDTO> RotateAsync(string imageData, int containerWidth, int containerHeight, double degrees);
    }
}
