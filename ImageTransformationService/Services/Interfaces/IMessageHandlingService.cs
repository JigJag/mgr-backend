﻿using System.Threading.Tasks;

namespace ImageTransformationService.Services.Interfaces
{
    public interface IMessageHandlingService
    {
        Task<string> HandleMessage(string msg, string type);
    }
}
