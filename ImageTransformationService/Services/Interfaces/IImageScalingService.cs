﻿
using ImageTransformationService.DTO;
using System.Threading.Tasks;

namespace ImageTransformationService.Services.Interfaces
{
    public interface IImageScalingService
    {
        Task<BasicResponseDTO> InitialScaleAsync(string imageData, int containerWidth, int containerHeight);
        Task<BasicResponseDTO> ScaleAsync(string imageData, int containerWidth, int containerHeight, double percent);
    }
}
