﻿
using ImageTransformationService.DTO;
using System.Threading.Tasks;

namespace ImageTransformationService.Services.Interfaces
{
    public interface IImageCroppingService
    {
        Task<BasicResponseDTO> CropAsync(string imageData, int x, int y, int width, int height);
    }
}
