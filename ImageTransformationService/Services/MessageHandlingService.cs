﻿using ImageTransformationService.DTO;
using ImageTransformationService.Services.Interfaces;
using System.Text.Json;
using System.Threading.Tasks;

namespace ImageTransformationService.Services
{
    public class MessageHandlingService : IMessageHandlingService
    {
        IImageScalingService _scalingService;
        IImageRotationService _rotationService;
        IImageCroppingService _croppingService;

        public MessageHandlingService(IImageScalingService imageScalingService, IImageRotationService imageRotationService,
                                         IImageCroppingService imageCroppingService)
        {
            _scalingService = imageScalingService;
            _rotationService = imageRotationService;
            _croppingService = imageCroppingService;
        }
        public async Task<string> HandleMessage(string msg, string type)
        {
            string result;
            BasicResponseDTO response;
            switch (type)
            {
                case "Initial":
                    InitialScaleDTO initial = JsonSerializer.Deserialize<InitialScaleDTO>(msg);
                    response = await _scalingService.InitialScaleAsync(initial.ImageData, initial.ContainerWidth, initial.ContainerHeight);
                    result = JsonSerializer.Serialize(response);
                    break;

                case "Scale":
                    ScaleDTO scale = JsonSerializer.Deserialize<ScaleDTO>(msg);
                    response = await _scalingService.ScaleAsync(scale.ImageData, scale.ContainerWidth, scale.ContainerHeight, scale.Percent);
                    result = JsonSerializer.Serialize(response);
                    break;

                case "Rotate":
                    RotateDTO rotate = JsonSerializer.Deserialize<RotateDTO>(msg);
                    response = await _rotationService.RotateAsync(rotate.ImageData, rotate.ContainerWidth, rotate.ContainerHeight, rotate.Degrees);
                    result = JsonSerializer.Serialize(response);
                    break;

                case "Crop":
                    CropDTO crop = JsonSerializer.Deserialize<CropDTO>(msg);
                    response = await _croppingService.CropAsync(crop.ImageData, crop.X, crop.Y, crop.Width, crop.Height);
                    result = JsonSerializer.Serialize(response);
                    break;

                default:
                    result = "";
                    break;
            }
            return result;
        }
    }
}
