﻿using ImageMagick;
using ImageTransformationService.DTO;
using ImageTransformationService.Services.Interfaces;
using ImageTransformationService.Utils;
using System;
using System.Threading.Tasks;

namespace ImageTransformationService.Models
{
    public class ImageCroppingService : IImageCroppingService
    {
        public Task<BasicResponseDTO> CropAsync(string imageData, int x, int y, int width, int height)
        {
            try
            {
                ImageProcessor imp = new ImageProcessor(imageData);
                MagickImage img = new MagickImage(imp.Data);
                img.Crop(new MagickGeometry(x, y, width, height), Gravity.Northwest);
                img.RePage();
                return Task.FromResult(new BasicResponseDTO { Image = imp.GetImageBase64(img), Ok = true, Message = "" });
            }
            catch (Exception e)
            {
                return Task.FromResult(new BasicResponseDTO { Image = "", Ok = false, Message = e.Message });
            }
            
        }
    }
}
