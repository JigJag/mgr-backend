﻿using ImageMagick;
using ImageTransformationService.DTO;
using ImageTransformationService.Services.Interfaces;
using ImageTransformationService.Utils;
using System;
using System.Threading.Tasks;

namespace ImageTransformationService.Models
{
    public class ImageRotationService : IImageRotationService

    {
        public Task<BasicResponseDTO> RotateAsync(string imageData, int containerWidth, int containerHeight, double degrees)
        {
            try
            {
                ImageProcessor imp = new ImageProcessor(imageData);
                MagickImage img = new MagickImage(imp.Data);
                if (img.Format != MagickFormat.Png)
                {
                    img.Format = MagickFormat.Png;
                }
                img.BackgroundColor = MagickColor.FromRgba(255, 255, 255, 0);
                img.Rotate(degrees);
                img.Trim();
                if (img.Width > containerWidth)
                    img.Resize(containerWidth, 0);
                else if (img.Height > containerHeight)
                    img.Resize(0, containerHeight);
                img.RePage();
                return Task.FromResult(new BasicResponseDTO { Image = imp.GetImageBase64(img), Ok = true, Message = "" });
            }
            catch (Exception e)
            {
                return Task.FromResult(new BasicResponseDTO { Image = "", Ok = false, Message = e.Message });
            }
            
        }
    }
}
