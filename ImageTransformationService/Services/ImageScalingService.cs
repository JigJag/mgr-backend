﻿using ImageMagick;
using ImageTransformationService.DTO;
using ImageTransformationService.Services.Interfaces;
using ImageTransformationService.Utils;
using System;
using System.Threading.Tasks;

namespace ImageTransformationService.Models
{
    public class ImageScalingService : IImageScalingService
    {
        public Task<BasicResponseDTO> InitialScaleAsync(string imageData, int containerWidth, int containerHeight)
        {
            ImageProcessor imp = new ImageProcessor(imageData);
            try
            {
                MagickImage img = new MagickImage(imp.Data);
                if (img.Width > containerWidth)
                {
                    img.Resize(containerWidth, 0);
                }
                if (img.Height > containerHeight)
                {
                    img.Resize(0, containerHeight);
                }
                return Task.FromResult(new BasicResponseDTO { Image = imp.GetImageBase64(img), Ok = true, Message = "" });
            }
            catch (Exception e)
            {
                return Task.FromResult(new BasicResponseDTO { Image = "", Ok = false, Message = e.Message });
            }
            
            
        }

        public Task<BasicResponseDTO> ScaleAsync(string imageData, int containerWidth, int containerHeight, double percent)
        {
            ImageProcessor imp = new ImageProcessor(imageData);
            try
            {
                MagickImage img = new MagickImage(imp.Data);
                if ((img.Width * percent / 100) > containerWidth)
                    img.Resize(containerWidth, 0);
                else if ((img.Height * percent / 100) > containerHeight)
                    img.Resize(0, containerHeight);
                else
                    img.Scale(new Percentage(percent));
                return Task.FromResult(new BasicResponseDTO { Image = imp.GetImageBase64(img), Ok = true, Message = "" });
            }
            catch (Exception e)
            {
                return Task.FromResult(new BasicResponseDTO { Image = "", Ok = false, Message = e.Message });
            }
            
        }
    }
}
