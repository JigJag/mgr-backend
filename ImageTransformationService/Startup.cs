using ImageTransformationService.Models;
using ImageTransformationService.Services;
using ImageTransformationService.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ImageTransformationService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IImageScalingService, ImageScalingService>();
            services.AddTransient<IImageRotationService, ImageRotationService>();
            services.AddTransient<IImageCroppingService, ImageCroppingService>();
            services.AddTransient<IMessageHandlingService, MessageHandlingService>();
            services.AddSingleton<IRabbitMQService, RabbitMQService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IRabbitMQService rabbitMQService)
        {
            rabbitMQService.PrepareConnection();
            rabbitMQService.Receive();
        }
    }
}
