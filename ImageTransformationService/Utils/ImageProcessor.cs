﻿using ImageMagick;
using System;

namespace ImageTransformationService.Utils
{
    public class ImageProcessor
    {
        public string Format { get; set; }
        public int Offset { get; set; }
        public byte[] Data { get; set; }

        public ImageProcessor(string DataURI)
        {
            Format = DataURI.Split(',')[0];
            Offset = DataURI.IndexOf(',') + 1;
            Data = Convert.FromBase64String(DataURI[Offset..^0]);
        }

        public string GetImageBase64(MagickImage img)
        {
            return Format + ',' + img.ToBase64();
        }


    }
}
