﻿
namespace ImageTextOverlayService.DTO
{
    public class ApplyTextDTO
    {
        public string ImageData { get; set; }
        public string UpperText { get; set; }
        public string LowerText { get; set; }
    }
}
