﻿
namespace ImageTextOverlayService.Services.Interfaces
{
    public interface IRabbitMQService
    {
        void PrepareConnection();
        void Send(string body, string correlationID);
        void Receive();
    }
}
