﻿
using ImageTextOverlayService.DTO;
using System.Threading.Tasks;

namespace ImageTextOverlayService.Services.Interfaces
{
    public interface ITextOverlayService
    {
        Task<BasicResponseDTO> ApplyText(string imageData, string upperText, string lowerText);
    }
}
