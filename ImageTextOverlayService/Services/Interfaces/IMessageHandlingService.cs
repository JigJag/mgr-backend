﻿using System.Threading.Tasks;

namespace ImageTextOverlayService.Services.Interfaces
{
    public interface IMessageHandlingService
    {
        Task<string> HandleMessage(string msg, string type);
    }
}
