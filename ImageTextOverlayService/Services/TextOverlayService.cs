﻿using ImageMagick;
using ImageTextOverlayService.DTO;
using ImageTextOverlayService.Services.Interfaces;
using ImageTextOverlayService.Utils;
using System;
using System.Threading.Tasks;

namespace ImageTextOverlayService.Services
{
    public class TextOverlayService : ITextOverlayService
    {
        public Task<BasicResponseDTO> ApplyText(string imageData, string upperText, string lowerText)
        {
            try
            {
                ImagePreProcessor imp = new ImagePreProcessor(imageData);
                MagickImage img = new MagickImage(imp.Data);
                int textSize = (140 * img.Width) / 1920;
                var txt = new Drawables()
                    .Font("DejaVu Sans", FontStyleType.Bold, FontWeight.UltraBold, FontStretch.Normal)
                    .FontPointSize(textSize)
                    .StrokeColor(MagickColor.FromRgba(0, 0, 0, 255))
                    .StrokeWidth((7 * img.Width) / 1920)
                    .FillColor(MagickColor.FromRgba(255, 255, 255, 255))
                    .TextAlignment(TextAlignment.Center)
                    .Text(img.Width / 2, img.Height * 0.15, upperText);
                img.Draw(txt);
                txt.Text(img.Width / 2, img.Height * 0.9, lowerText);
                img.Draw(txt);
                return Task.FromResult(new BasicResponseDTO { Image = imp.GetImageBase64(img), Ok = true, Message = "" });
            }
            catch (Exception e)
            {
                return Task.FromResult(new BasicResponseDTO { Image = "", Ok = false, Message = e.Message });
            }
        }
    }
}
