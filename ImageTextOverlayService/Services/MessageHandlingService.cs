﻿using ImageTextOverlayService.DTO;
using ImageTextOverlayService.Services.Interfaces;
using System.Text.Json;
using System.Threading.Tasks;

namespace ImageTextOverlayService.Services
{
    public class MessageHandlingService : IMessageHandlingService
    {
        ITextOverlayService _textOverlay;

        public MessageHandlingService(ITextOverlayService textOverlayService)
        {
            _textOverlay = textOverlayService;
        }
        public async Task<string> HandleMessage(string msg, string type)
        {
            string result;
            BasicResponseDTO response;
            switch (type)
            {
                case "ApplyText":
                    ApplyTextDTO text = JsonSerializer.Deserialize<ApplyTextDTO>(msg);
                    response = await _textOverlay.ApplyText(text.ImageData, text.UpperText, text.LowerText);
                    result = JsonSerializer.Serialize(response);
                    break;

                default:
                    result = "";
                    break;
            }
            return result;
        }
    }
}
