using ImageTextOverlayService.Services;
using ImageTextOverlayService.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ImageTextOverlayService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddDefaultPolicy(builder =>
            {
                builder
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .WithMethods("OPTIONS", "POST", "GET");
            }));

            services.AddTransient<ITextOverlayService, TextOverlayService>();
            services.AddTransient<IMessageHandlingService, MessageHandlingService>();
            services.AddSingleton<IRabbitMQService, RabbitMQService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IRabbitMQService rabbitMQService)
        {
            rabbitMQService.PrepareConnection();
            rabbitMQService.Receive();
        }
    }
}
