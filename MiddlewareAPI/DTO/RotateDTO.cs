﻿
namespace MiddlewareAPI.DTO
{
    public class RotateDTO
    {
        public string ImageData { get; set; }
        public int ContainerWidth { get; set; }
        public int ContainerHeight { get; set; }
        public double Degrees { get; set; }
    }
}
