﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiddlewareAPI.DTO
{
    public class BasicResponseDTO
    {
        public string Image { get; set; }
        public bool Ok { get; set; }
        public string Message { get; set; }
    }
}
