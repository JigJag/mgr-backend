﻿
namespace MiddlewareAPI.DTO
{
    public class InitialScaleDTO
    {
        public string ImageData { get; set; }
        public int ContainerWidth { get; set; }
        public int ContainerHeight { get; set; }
    }
}
