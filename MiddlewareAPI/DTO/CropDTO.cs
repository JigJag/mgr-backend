﻿
namespace MiddlewareAPI.DTO
{
    public class CropDTO
    {
        public string ImageData { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
