﻿
namespace MiddlewareAPI.DTO
{
    public class LightDimContrastSepiaDTO
    {
        public string ImageData { get; set; }
        public double Value { get; set; }
    }
}
