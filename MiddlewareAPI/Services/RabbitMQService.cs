﻿using Microsoft.Extensions.Configuration;
using MiddlewareAPI.Services.Interfaces;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using System;
using System.Text;
using System.Threading.Tasks;

namespace MiddlewareAPI.Services
{
    public class RabbitMQService : IRabbitMQService
    {
        IConfiguration _conf;
        IConnection _conn;
        IModel _chnl;
        string Expire = "8000";
        public RabbitMQService(IConfiguration configuration)
        {
            _conf = configuration;
        }

        public void Send(string service, string operation, string correlationId, string body)
        {
            using (var channel = _conn.CreateModel())
            {
                channel.QueueDeclare(queue: service,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
                
                var msg = Encoding.UTF8.GetBytes(body);

                IBasicProperties props = channel.CreateBasicProperties();
                props.CorrelationId = correlationId;
                props.Type = operation;
                props.Expiration = Expire;
                channel.BasicPublish(exchange: "",
                                     routingKey: service,
                                     basicProperties: props,
                                     body: msg);
                channel.Close();
            }
        }

        public Task<string> Receive(string queue, string correlationId)
        {
            _chnl = _conn.CreateModel();
            _chnl.QueueDeclare(queue: queue,
                               durable: false,
                               exclusive: false,
                               autoDelete: false,
                               arguments: null);

            var consumer = new EventingBasicConsumer(_chnl);
            string msg = "";
            var tcs = new TaskCompletionSource<string>();
            consumer.Received += (model, ea) =>
            {
                Console.WriteLine("Something came...");
                if (ea.BasicProperties.CorrelationId == correlationId)
                {
                    Console.WriteLine("... it came to me !");
                    var body = ea.Body;
                    msg = Encoding.UTF8.GetString(body.ToArray());
                    _chnl.Close();
                    tcs.TrySetResult(msg);
                }

            };
            _chnl.BasicConsume(queue, true, consumer);
            return tcs.Task;
        }

        public void PrepareConnection()
        {
            try
            {
                if(Environment.GetCommandLineArgs().Length == 5)
                {
                    var factory = new ConnectionFactory()
                    {
                        HostName = Environment.GetCommandLineArgs()[1],
                        Port = int.Parse(Environment.GetCommandLineArgs()[2]),
                        UserName = Environment.GetCommandLineArgs()[3],
                        Password = Environment.GetCommandLineArgs()[4]
                    };
                    _conn = factory.CreateConnection();
                    Console.WriteLine($"Connected to Wabbit on {factory.HostName} : {factory.Port} ...");
                }
                else
                {
                    var factory = new ConnectionFactory()
                    {
                        HostName = _conf.GetSection("Rabbit").GetValue<string>("Hostname"),
                        Port = _conf.GetSection("Rabbit").GetValue<int>("Port"),
                        UserName = _conf.GetSection("Rabbit").GetValue<string>("Username"),
                        Password = _conf.GetSection("Rabbit").GetValue<string>("Password")
                    };
                    _conn = factory.CreateConnection();
                    Console.WriteLine($"Connected to Wabbit on {factory.HostName} : {factory.Port} ...");
                }
            }
            catch (BrokerUnreachableException e)
            {
                throw new Exception("Cannot connect to Broker!");
            }
            
        }
    }
}
