﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace MiddlewareAPI.Services.Interfaces
{
    public interface IRabbitMQService
    {
        void PrepareConnection();
        void Send(string service, string operation, string correlationId, string body);
        Task<string> Receive(string queue, string correlationId);
    }
}
