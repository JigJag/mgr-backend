﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MiddlewareAPI.DTO;
using MiddlewareAPI.Services.Interfaces;

namespace MiddlewareAPI.Controllers
{
    [Route("api")]
    [ApiController]
    public class APIController : ControllerBase
    {
        IRabbitMQService _rabbit;
        int Timeout = 8000;

        public APIController(IRabbitMQService rabbitMQService)
        {
            _rabbit = rabbitMQService;
        }

        [HttpPost("Initial")]
        public async Task<IActionResult> ScaleInitially([FromBody] InitialScaleDTO data)
        {
            string correlationId = Guid.NewGuid().ToString();
            string msg = JsonSerializer.Serialize(data);
            _rabbit.Send("Transformations", "Initial", correlationId, msg);
            return Ok(await Receive(Timeout, correlationId));
        }

        [HttpPost("Scale")]
        public async Task<IActionResult> Scale([FromBody] ScaleDTO data)
        {
            string correlationId = Guid.NewGuid().ToString();
            string msg = JsonSerializer.Serialize(data);
            _rabbit.Send("Transformations", "Scale", correlationId, msg);
            return Ok(await Receive(Timeout, correlationId));
        }

        [HttpPost("Rotate")]
        public async Task<IActionResult> Rotate([FromBody] RotateDTO data)
        {
            string correlationId = Guid.NewGuid().ToString();
            string msg = JsonSerializer.Serialize(data);
            _rabbit.Send("Transformations", "Rotate", correlationId, msg);
            return Ok(await Receive(Timeout, correlationId));
        }

        [HttpPost("Crop")]
        public async Task<IActionResult> Crop([FromBody] CropDTO data)
        {
            string correlationId = Guid.NewGuid().ToString();
            string msg = JsonSerializer.Serialize(data);
            _rabbit.Send("Transformations", "Crop", correlationId, msg);
            return Ok(await Receive(Timeout, correlationId));
        }

        [HttpPost("ApplyText")]
        public async Task<IActionResult> TextOverlay([FromBody] ApplyTextDTO data)
        {
            string correlationId = Guid.NewGuid().ToString();
            string msg = JsonSerializer.Serialize(data);
            _rabbit.Send("TextOverlay", "ApplyText", correlationId, msg);
            return Ok(await Receive(Timeout, correlationId));
        }

        [HttpPost("Blur")]
        public async Task<IActionResult> Blur([FromBody] BlurSharpenDTO data)
        {
            string correlationId = Guid.NewGuid().ToString();
            string msg = JsonSerializer.Serialize(data);
            _rabbit.Send("Filtering", "Blur", correlationId, msg);
            return Ok(await Receive(Timeout, correlationId));
        }

        [HttpPost("Contrast")]
        public async Task<IActionResult> Contrast([FromBody] LightDimContrastSepiaDTO data)
        {
            string correlationId = Guid.NewGuid().ToString();
            string msg = JsonSerializer.Serialize(data);
            _rabbit.Send("Filtering", "Contrast", correlationId, msg);
            return Ok(await Receive(Timeout, correlationId));
        }

        [HttpPost("Dim")]
        public async Task<IActionResult> Dim([FromBody] LightDimContrastSepiaDTO data)
        {
            string correlationId = Guid.NewGuid().ToString();
            string msg = JsonSerializer.Serialize(data);
            _rabbit.Send("Filtering", "Dim", correlationId, msg);
            return Ok(await Receive(Timeout, correlationId));
        }

        [HttpPost("Light")]
        public async Task<IActionResult> Light([FromBody] LightDimContrastSepiaDTO data)
        {
            string correlationId = Guid.NewGuid().ToString();
            string msg = JsonSerializer.Serialize(data);
            _rabbit.Send("Filtering", "Light", correlationId, msg);
            return Ok(await Receive(Timeout, correlationId));
        }

        [HttpPost("Sepia")]
        public async Task<IActionResult> Sepia([FromBody] LightDimContrastSepiaDTO data)
        {
            string correlationId = Guid.NewGuid().ToString();
            string msg = JsonSerializer.Serialize(data);
            _rabbit.Send("Filtering", "Sepia", correlationId, msg);
            return Ok(await Receive(Timeout, correlationId));
        }

        [HttpPost("Sharpen")]
        public async Task<IActionResult> Sharpen([FromBody] BlurSharpenDTO data)
        {
            string correlationId = Guid.NewGuid().ToString();
            string msg = JsonSerializer.Serialize(data);
            _rabbit.Send("Filtering", "Sharpen", correlationId, msg);
            return Ok(await Receive(Timeout, correlationId));
        }

        [HttpPost("Vignette")]
        public async Task<IActionResult> Vignette([FromBody] VignetteDTO data)
        {
            string correlationId = Guid.NewGuid().ToString();
            string msg = JsonSerializer.Serialize(data);
            _rabbit.Send("Filtering", "Vignette", correlationId, msg);
            return Ok(await Receive(Timeout, correlationId));
        }

        public async Task<BasicResponseDTO> Receive(int timeout, string correlationId)
        {
            var task = _rabbit.Receive("Processed", correlationId);
            if (await Task.WhenAny(task, Task.Delay(timeout)) == task)
            {
                return JsonSerializer.Deserialize<BasicResponseDTO>(task.Result);
            }
            else
            {
                return new BasicResponseDTO { Image="", Message="Serwis niedostępny", Ok=false };
            }
            
        }
    }
}