﻿
namespace ImageFilteringService.DTO
{
    public class BlurSharpenDTO
    {
        public string ImageData { get; set; }
        public double Value { get; set; }
    }
}
