﻿
namespace ImageFilteringService.DTO
{
    public class VignetteDTO
    {
        public string ImageData { get; set; }
        public double Radius { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Color { get; set; }
    }
}
