﻿using ImageFilteringService.DTO;
using ImageFilteringService.Services.Interafecs;
using ImageFilteringService.Utils;
using ImageMagick;
using System;
using System.Threading.Tasks;

namespace ImageFilteringService.Services
{
    public class ImageVignetteService : IImageVignetteService
    {
        public Task<BasicResponseDTO> Vignette(string imageData, double radius, int width, int height, string color)
        {
            try
            {
                ImagePreProcessor imp = new ImagePreProcessor(imageData);
                MagickImage img = new MagickImage(imp.Data);
                if (color == "black")
                {
                    img.BackgroundColor = MagickColor.FromRgba(0, 0, 0, 220);
                }
                else
                {
                    img.BackgroundColor = MagickColor.FromRgba(255, 255, 255, 220);
                }
                img.Vignette(radius, radius, (img.Width / 2 * width / 100), (img.Height / 2 * height / 100));
                return Task.FromResult(new BasicResponseDTO { Image = imp.GetImageBase64(img), Ok = true, Message = "" });
            }
            catch (Exception e)
            {
                return Task.FromResult(new BasicResponseDTO { Image = "", Ok = false, Message = e.Message });
            }
            
        }
    }
}
