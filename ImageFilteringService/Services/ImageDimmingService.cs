﻿using ImageFilteringService.DTO;
using ImageFilteringService.Services.Interafecs;
using ImageFilteringService.Utils;
using ImageMagick;
using System;
using System.Threading.Tasks;

namespace ImageFilteringService.Services
{
    public class ImageDimmingService : IImageDimmingService
    {
        public Task<BasicResponseDTO> Dim(string imageData, double alpha)
        {
            try
            {
                ImagePreProcessor imp = new ImagePreProcessor(imageData);
                MagickImage img = new MagickImage(imp.Data);
                img.Colorize(MagickColor.FromRgb(0, 0, 0), new Percentage(alpha));
                return Task.FromResult(new BasicResponseDTO { Image = imp.GetImageBase64(img), Ok = true, Message = "" });
            }
            catch (Exception e)
            {
                return Task.FromResult(new BasicResponseDTO { Image = "", Ok = false, Message = e.Message });
            }
            
        }
    }
}
