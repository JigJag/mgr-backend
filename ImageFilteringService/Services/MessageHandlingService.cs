﻿using ImageFilteringService.DTO;
using ImageFilteringService.Services.Interafecs;
using ImageFilteringService.Services.Interfaces;
using System.Text.Json;
using System.Threading.Tasks;

namespace ImageFilteringService.Services
{
    public class MessageHandlingService : IMessageHandlingService
    {
        IImageBlurringService _blur;
        IImageContrastService _contrast;
        IImageDimmingService _dim;
        IImageLightingService _light;
        IImageSepiaService _sepia;
        IImageSharpeningService _sharpening;
        IImageVignetteService _vignette;

        public MessageHandlingService(IImageBlurringService imageBlurringService, IImageContrastService imageContrastService,
                                      IImageDimmingService imageDimmingService, IImageLightingService imageLightingService,
                                      IImageSepiaService imageSepiaService, IImageSharpeningService imageSharpeningService,
                                       IImageVignetteService imageVignetteService)
        {
            _blur = imageBlurringService;
            _contrast = imageContrastService;
            _dim = imageDimmingService;
            _light = imageLightingService;
            _sepia = imageSepiaService;
            _sharpening = imageSharpeningService;
            _vignette = imageVignetteService;
        }
        public async Task<string> HandleMessage(string msg, string type)
        {
            string result;
            BasicResponseDTO response;
            switch (type)
            {
                case "Blur":
                    BlurSharpenDTO blur = JsonSerializer.Deserialize<BlurSharpenDTO>(msg);
                    response = await _blur.Blur(blur.ImageData, blur.Value);
                    result = JsonSerializer.Serialize(response);
                    break;

                case "Contrast":
                    LightDimContrastSepiaDTO contrast = JsonSerializer.Deserialize<LightDimContrastSepiaDTO>(msg);
                    response = await _contrast.Contrast(contrast.ImageData, contrast.Value);
                    result = JsonSerializer.Serialize(response);
                    break;

                case "Dim":
                    LightDimContrastSepiaDTO dim = JsonSerializer.Deserialize<LightDimContrastSepiaDTO>(msg);
                    response = await _dim.Dim(dim.ImageData, dim.Value);
                    result = JsonSerializer.Serialize(response);
                    break;

                case "Light":
                    LightDimContrastSepiaDTO light = JsonSerializer.Deserialize<LightDimContrastSepiaDTO>(msg);
                    response = await _light.Enlight(light.ImageData, light.Value);
                    result = JsonSerializer.Serialize(response);
                    break;

                case "Sepia":
                    LightDimContrastSepiaDTO sepia = JsonSerializer.Deserialize<LightDimContrastSepiaDTO>(msg);
                    response = await _sepia.Sepia(sepia.ImageData, sepia.Value);
                    result = JsonSerializer.Serialize(response);
                    break;

                case "Sharpen":
                    BlurSharpenDTO sharpen = JsonSerializer.Deserialize<BlurSharpenDTO>(msg);
                    response = await _sharpening.Sharpen(sharpen.ImageData, sharpen.Value);
                    result = JsonSerializer.Serialize(response);
                    break;

                case "Vignette":
                    VignetteDTO vignette = JsonSerializer.Deserialize<VignetteDTO>(msg);
                    response = await _vignette.Vignette(vignette.ImageData, vignette.Radius, vignette.Width, vignette.Height, vignette.Color);
                    result = JsonSerializer.Serialize(response);
                    break;

                default:
                    result = "";
                    break;
            }
            return result;
        }
    }
}
