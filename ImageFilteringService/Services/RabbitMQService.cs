﻿using ImageFilteringService.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using System;
using System.Text;

namespace ImageFilteringService.Services
{
    public class RabbitMQService : IRabbitMQService
    {
        IConfiguration _conf;
        IMessageHandlingService _messageHandler;
        IConnection _conn;
        IModel _chnl;
        string Expire = "20000";
        public RabbitMQService(IConfiguration configuration, IMessageHandlingService messageHandlingService)
        {
            _conf = configuration;
            _messageHandler = messageHandlingService;
        }

        public void Send(string body, string correlationID)
        {
            using (var channel = _conn.CreateModel())
            {
                var msg = Encoding.UTF8.GetBytes(body);
                IBasicProperties props = channel.CreateBasicProperties();
                props.CorrelationId = correlationID;
                props.Expiration = Expire;
                channel.QueueDeclare(queue: "Processed",
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);
                channel.BasicPublish(exchange: "",
                                     routingKey: "Processed",
                                     basicProperties: props,
                                     body: msg);
            }
        }

        public void PrepareConnection()
        {
            try
            {
                if (Environment.GetCommandLineArgs().Length == 5)
                {
                    var factory = new ConnectionFactory()
                    {
                        HostName = Environment.GetCommandLineArgs()[1],
                        Port = int.Parse(Environment.GetCommandLineArgs()[2]),
                        UserName = Environment.GetCommandLineArgs()[3],
                        Password = Environment.GetCommandLineArgs()[4]
                    };
                    _conn = factory.CreateConnection();
                    Console.WriteLine($"Connected to Wabbit on {factory.HostName} : {factory.Port} ...");
                }
                else
                {
                    var factory = new ConnectionFactory()
                    {
                        HostName = _conf.GetSection("Rabbit").GetValue<string>("Hostname"),
                        Port = _conf.GetSection("Rabbit").GetValue<int>("Port"),
                        UserName = _conf.GetSection("Rabbit").GetValue<string>("Username"),
                        Password = _conf.GetSection("Rabbit").GetValue<string>("Password")
                    };
                    _conn = factory.CreateConnection();
                    Console.WriteLine($"Connected to Wabbit on {factory.HostName} : {factory.Port} ...");
                }
            }
            catch (BrokerUnreachableException e)
            {
                Console.WriteLine("Cannot connect to Broker !");
                return;
            }
        }

        public void Receive()
        {
            _chnl = _conn.CreateModel();
            try
            {
                _chnl.QueueDeclare(queue: "Filtering",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                _chnl.BasicQos(0, 3, false);
                var consumer = new EventingBasicConsumer(_chnl);
                consumer.Received += async (model, ea) =>
                {
                    Console.WriteLine("Got message !");
                    var msg = ea.Body;
                    var body = Encoding.UTF8.GetString(msg.ToArray());
                    var result = await _messageHandler.HandleMessage(body, ea.BasicProperties.Type);
                    Send(result, ea.BasicProperties.CorrelationId);
                };
                _chnl.BasicConsume("Filtering", true, consumer);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
