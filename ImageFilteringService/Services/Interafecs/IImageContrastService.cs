﻿
using ImageFilteringService.DTO;
using System.Threading.Tasks;

namespace ImageFilteringService.Services.Interafecs
{
    public interface IImageContrastService
    {
        Task<BasicResponseDTO> Contrast(string imageData, double sigma);
    }
}
