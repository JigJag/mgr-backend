﻿
using ImageFilteringService.DTO;
using System.Threading.Tasks;

namespace ImageFilteringService.Services.Interafecs
{
    public interface IImageSharpeningService
    {
        Task<BasicResponseDTO> Sharpen(string imageData, double sigma);
    }
}
