﻿using System.Threading.Tasks;

namespace ImageFilteringService.Services.Interfaces
{
    public interface IMessageHandlingService
    {
        Task<string> HandleMessage(string msg, string type);
    }
}
