﻿
using ImageFilteringService.DTO;
using System.Threading.Tasks;

namespace ImageFilteringService.Services.Interafecs
{
    public interface IImageDimmingService
    {
        Task<BasicResponseDTO> Dim(string imageData, double sigma);
    }
}
