﻿
using ImageFilteringService.DTO;
using System.Threading.Tasks;

namespace ImageFilteringService.Services.Interafecs
{
    public interface IImageLightingService
    {
        Task<BasicResponseDTO> Enlight(string imageData, double sigma);
    }
}
