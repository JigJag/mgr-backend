﻿
using ImageFilteringService.DTO;
using System.Threading.Tasks;

namespace ImageFilteringService.Services.Interafecs
{
    public interface IImageVignetteService
    {
        Task<BasicResponseDTO> Vignette(string imageData, double radius, int width, int height, string color);
    }
}
