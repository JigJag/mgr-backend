﻿
using ImageFilteringService.DTO;
using System.Threading.Tasks;

namespace ImageFilteringService.Services.Interafecs
{
    public interface IImageSepiaService
    {
        Task<BasicResponseDTO> Sepia(string imageData, double sigma);
    }
}
