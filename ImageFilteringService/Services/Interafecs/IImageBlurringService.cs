﻿
using ImageFilteringService.DTO;
using System.Threading.Tasks;

namespace ImageFilteringService.Services.Interafecs
{
    public interface IImageBlurringService
    {
        Task<BasicResponseDTO> Blur(string imageData, double sigma);
    }
}
