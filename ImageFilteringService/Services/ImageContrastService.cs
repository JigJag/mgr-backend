﻿using ImageFilteringService.DTO;
using ImageFilteringService.Services.Interafecs;
using ImageFilteringService.Utils;
using ImageMagick;
using System;
using System.Threading.Tasks;

namespace ImageFilteringService.Services
{
    public class ImageContrastService : IImageContrastService
    {
        public Task<BasicResponseDTO> Contrast(string imageData, double sigma)
        {
            try
            {
                ImagePreProcessor imp = new ImagePreProcessor(imageData);
                MagickImage img = new MagickImage(imp.Data);
                img.SigmoidalContrast(sigma);
                return Task.FromResult(new BasicResponseDTO { Image = imp.GetImageBase64(img), Ok = true, Message = "" });
            }
            catch (Exception e)
            {
                return Task.FromResult(new BasicResponseDTO { Image = "", Ok = false, Message = e.Message });
            }
            
        }
    }
}
