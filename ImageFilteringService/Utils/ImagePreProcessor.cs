﻿using ImageMagick;
using System;

namespace ImageFilteringService.Utils
{
    public class ImagePreProcessor
    {
        public string Format { get; set; }
        public int Offset { get; set; }
        public byte[] Data { get; set; }

        public ImagePreProcessor(string DataURI)
        {
            Format = DataURI.Split(',')[0];
            Offset = DataURI.IndexOf(',') + 1;
            Data = Convert.FromBase64String(DataURI[Offset..^0]);
        }

        public string GetImageBase64(MagickImage img)
        {
            return Format + ',' + img.ToBase64();
        }


    }
}
